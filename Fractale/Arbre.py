
import matplotlib

%matplotlib notebook


#%%

import numpy as np
import matplotlib.pyplot as plt

#%%


def radian(a):
    return a*np.pi/180



def arbre(x,y,l,e,a,n=5):
    x1=x+np.cos(radian(a))*l
    y1=y+np.sin(radian(a))*l
    plt.plot([x,x1],[y,y1],"-",color="black",linewidth=e)
    if n==0:
        return
    arbre(x1,y1,l*0.8,e*0.8,a+20,n-1)
    arbre(x1,y1,l*0.8,e*0.8,a-20,n-1)



# %%
plt.figure(figsize=(10,10))

# plt.subplot(1,1,1)
plt.xlim(-200,200)
plt.ylim(0,400)

#arbre(0,0,100,20,90)

plt.plot([0,100],[0,100])
plt.show()
# %%




plt.plot([0,100],[0,100])
plt.show()
# %%
