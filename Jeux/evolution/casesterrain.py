
# coding: utf-8

# In[1]:


import tkinter as TK
from numpy import cos,sin
from numpy import pi as PI
import numpy as np


# In[6]:


class Tuile(TK.Canvas):
    
    taille=200
    
    def __init__(self,master=None,L=[]):   # L = [[[color,[x1,y1,x2,y2]],...]]]
        
        w=Tuile.taille
        h=Tuile.taille
        
        TK.Canvas.__init__(self, master=master, width=w, height=h, background="green", bd=1, highlightthickness=0, relief='ridge')
        
        self.L=[]
        for D in L:
            color=D[0]
            Lp=[xy for xy in D[1]]
            self.L+=[[color,Lp]]
            
        
        self.taille={'x':int(self.cget("width"))/2, 'y':int(self.cget("height"))/2}
        self.centre={'x':int(self.cget("width"))/2, 'y':int(self.cget("height"))/2}
        
        
    def add(self,color,polygon):
        self.L+=[[color,polygon]]
        
        
    def dessine(self):
        for D in self.L:  # liste des points [x1,y1,x2,y2,...] pas besoin que dernier = 1er
            color=D[0]
            Lp=[]
            for i in range(int(len(D[1])/2)):
                x=D[1][2*i]
                y=D[1][2*i+1]
                Lp+=[x*self.taille['x']+self.centre['x'],-y*self.taille['y']+self.centre['y']]
            self.create_polygon(Lp,     outline='gray', fill=color, width=0)
        self.update()
            
    @staticmethod
    def Protate(L,angle,centre=(0,0)):
        LR=[]
        angle=angle*PI/180
        x0,y0=centre
        for i in range(int(len(L)/2)):
            x=L[2*i]
            y=L[2*i+1]
            LR+=[x0+(x-x0)*cos(angle)-(y-y0)*sin(angle),y0+(x-x0)*sin(angle)+(y-y0)*cos(angle)]
        return LR
    
    def rotate(self,angle=90,centre=(0,0)):
        for D in self.L:
            D[1]=Tuile.Protate(D[1],angle,centre)
            
    @staticmethod
    def blurline(n=15):  # trait horizontal flou de E vers O.
        I=np.linspace(-1.,1.,n)
        J=sin(I*PI*4)*np.random.random(n)
        return -I,J
    
    @staticmethod
    def trajdir(d="EO",h=0.25,bl=0.05):   # fabrique un trait flou allant d'une direction a une autre, a une hauteur h
        
        Ld={"E":0,"N":1,"O":2,"S":3}  # convertit direction cardinale en angle (x 90 degres)
        a0=Ld[d[0]] # angle de depart
        a1=Ld[d[1]] # angle final
        a=(a1-a0) % 4  # difference d'angle
        #print a0,a1,a
        # on va partir de l'E, et tourner de a. ensuite on tournera tout le monde de a0.
        if a==2:
            I,J=Tuile.blurline()
            X=I
            Y=bl*J+h
        if a==1:
            I,J=Tuile.blurline()
            A=(I+1)*PI/4
            R=(bl*J+1-h)
            X=-R*cos(A)+1
            Y=-R*sin(A)+1
        if a==3:
            I,J=Tuile.blurline()
            A=(I+1)*PI/4
            R=(bl*J+h)+1
            X=-R*cos(A)+1
            Y=R*sin(A)-1
        if a==0:
            I,J=Tuile.blurline()
            X1=(I+1)/2
            Y1=bl*J+h
            I,J=Tuile.blurline()
            A=(-I+2)*PI/2
            R=(bl*J+h)
            X2=R*cos(A)
            Y2=R*sin(A)
            I,J=Tuile.blurline()
            X3=(-I+1)/2
            Y3=bl*J-h
            X=np.concatenate([X1,X2,X3])
            Y=np.concatenate([Y1,Y2,Y3])
        L=[]
        for i in range(len(X)):
            L+=[X[i],Y[i]]
        L=Tuile.Protate(L,a0*90)
        return L        
        
    def add_riviere(self,d="OE"): 
        dinv=d[1]+d[0]
        if d[0]!=d[1]:
            L=Tuile.trajdir(d,0.25)+Tuile.trajdir(dinv,0.25)
            self.add("blue",L)
        else:
            L=Tuile.trajdir(d,0.25)
            self.add("blue",L)
            
                    

    def add_plage(self,d="OE"): 
        Ld={"E":0,"N":1,"O":2,"S":3}  # convertit direction cardinale en angle (x 90 degres)
        a0=Ld[d[0]] # angle de depart
        a1=Ld[d[1]] # angle final
        a=(a1-a0) % 4  # difference d'angle

        dinv=d[1]+d[0] # trajectoire inverse
        
        if a!=0:
            L1=Tuile.trajdir(d,-0.85)
            L2=Tuile.trajdir(dinv,0.65)            
            self.add("yellow",L1+L2)
            
            if a==2:
                L3=[-1,-1,1,-1]
            if a==1:
                L3=[-1,1,-1,-1,1,-1]
            if a==3:
                L3=[1,-1]
            self.add("darkblue",L1+Tuile.Protate(L3,a0*90))

            

        return 
    
    
        if d=="OE" or d=="EO" or d=="NS" or d=="SN":
            tourne=False
        else:
            tourne=True
        color="yellow"
        n=20
        I=np.linspace(-1.,1.,n)
        J=0.85+0.05*sin(I*PI*4)*np.random.random(n)
        if tourne:
            X=(J+1)*cos((I+1)*PI/4)-1
            Y=(J+1)*sin((I+1)*PI/4)-1
            XX=(J+.8)*cos((-I+1)*PI/4)-1
            YY=(J+.8)*sin((-I+1)*PI/4)-1
        else:
            X=I
            Y=J
            XX=-I
            YY=J-0.2
            
        L=[]
        for i in range(n):
            L+=[X[i],Y[i]]
        for i in range(n):
            L+=[XX[i],YY[i]]            
                
        if d=="NS" or d=="SN":
            L=Tuile.Protate(L,90)            
        if d=="ON" or d=="NO":
            L=Tuile.Protate(L,-90)
        if d=="SE" or d=="ES":
            L=Tuile.Protate(L,90)
        if d=="NE" or d=="EN":
            L=Tuile.Protate(L,180)
            
        self.add(color,L)

        color="darkblue"
        L=[]
        for i in range(n):
            L+=[X[i],Y[i]]
        L+=[1,1,-1,1]

        self.add(color,L)


# In[7]:


if __name__=="__main__":
    
    Fen=TK.Tk()
    Fen.attributes("-topmost", True)
    
    # Tuile.taille=200  # si on veut changer la taille des cases.


    T00=Tuile(Fen)
    T00.add_plage("OE")
    T00.add_riviere("OE")
    #T00.rotate(180)

    T10=Tuile(Fen)
    T10.add_riviere("OS")
    T10.add_plage("OS")

    T11=Tuile(Fen)
    T11.add_riviere("NN")
    T11.add_plage("NE")


    T00.grid(row=0, column=0, sticky="nsew")
    T10.grid(row=0, column=1, sticky="nsew")
    T11.grid(row=1, column=1, sticky="nsew")


    T00.dessine()
    T10.dessine()
    T11.dessine()

    Fen.mainloop()


