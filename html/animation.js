/// debut Animation

var reqanimationreference;
var starttime; 
var controlt;
var OK;



function anime(action,fin,ttotal){  // ttotal = temps total en ms, action(t/ttotal) -> true or false, fin() s'arete au bout de ttotal ou si action=false
    function startanim(ts){
        starttime = ts; 
        f=function(ts){
            var r=(ts-starttime)/ttotal;
            controlt=r;
            OK=action(r);
            if (OK && (r<1)){
                reqanimationreference=requestAnimationFrame(f);
            }
            else{fin()};
        }

        requestAnimationFrame(f);
    }
    requestAnimationFrame(startanim);

};

function stopanim(){
    cancelAnimationFrame(reqanimationreference);
}



function animet(action,fin,ttotal){  // ttotal = temps total en ms, action(t/ttotal), fin()
    function startanim(ts){
        starttime = ts; 
        f=function(ts){
            if (ts-starttime<ttotal){
                controlt=(ts-starttime)/ttotal;
                action((ts-starttime)/ttotal);
                reqanimationreference=requestAnimationFrame(f);
            }
            else{fin()};
        }

        requestAnimationFrame(f);
    }
    requestAnimationFrame(startanim);

};


