
# coding: utf-8

# In[35]:


try:
    from Tkinter import * # for Python2
except ImportError:
    from tkinter import * # for Python3

from numpy.random import randint


# In[36]:


# derniere edition: papa le 16 fevrier


# In[37]:


class DD:
    
    def __init__(self,vmax,vmin=1):
        self.max=vmax
        self.min=vmin
        self.valeur=vmax
        
    def tire(self):
        self.valeur=randint(self.max-self.min+1)+self.min
        return self.valeur
        


# In[38]:



#if __name__=="__main__":
#    D1=DD(6)
#    for i in range(10):
#        print D1.tire()



# In[39]:


class De(DD,Frame):
    
    def __init__(self,vmax,vmin=1,root=None,pack=LEFT,run=False):
        DD.__init__(self,vmax,vmin)
        if root==None:
            root=Tk()
            root.title(u"essai Dés")
            root.geometry("200x200")
        self.root=root
        
        Frame.__init__(self,master=root, bd=5,relief= GROOVE)
        
        if pack!=False:
            self.pack(side=pack)
        self.val = StringVar()        
        self.val.set(".")
        
        self.LabTxt=Label(self,text=u"Dé de ")
        self.LabTxt.grid(row=1,column=0)
        
        self.vm = StringVar()
        self.vm.set(str(vmax))
        #self.vm.trace("w", self.changevmax)
        
        self.Emax=Entry(self,width=3,textvariable=self.vm)
        self.Emax.grid(row=1,column=1)
        self.Emax.bind('<Leave>', self.changevmax)
        self.Emax.bind('<FocusOut>', self.changevmax)
        self.Emax.bind('<Return>', self.changevmax)
        
        self.LabDe=Label(self,textvariable=self.val,bg="yellow")
        self.LabDe.grid(row=2,column=0)
        self.LabDe.config(font=("Courier", 44))
        self.LabDe.config(width=3)
        
        self.Bquit=Button(self,text=u"⊠",command=self.close)
        self.Bquit.grid(row=3,column=0)
        
        self.BtireDe=Button(self,text="lance",command=self.lance)
        self.BtireDe.grid(row=3,column=1)
        
        if run:
            self.run()
        
    
    def lance(self):
        v=self.tire()
        self.val.set(str(v))
        
    def close(self):
        if isinstance(self.root,FenDes):
            self.root.closeDe(self)
            
        else:
            #self.root.pack_forget()
            self.destroy()
            
    def run(self):
        self.root.mainloop()
        
    def changevmax(self,*args):
        try:
            v=int(self.vm.get())
            self.max=v
            #FenDes.currentnumber=v
            #print("v="+str(v))
        except:
            self.vm.set(str(self.max))
            
    


# In[57]:


class FenDes(Tk):
    currentnumber=20
    
    def __init__(self,run=False):
        self.nbdesparligne=5
        
        
        Tk.__init__(self)
        
        newD=De(20,root=self,pack=False)
        newD.grid(row=1,column=0)
        self.LD=[newD]
        
        self.ButAddDe=Button(self,text="nouveau",command=self.addDen())
        self.ButAddDe.grid(row=0,column=0)

        self.Butlancetous=Button(self,text="lancer tous",command=self.lancetous)
        self.Butlancetous.grid(row=0,column=1)

        
        
        self.title(u"Dés")
        #self.geometry("400x300")


        # create a toplevel menu
        self.menubar = Menu(self)
        #self.menubar.add_command(label="Hello!", command=hello)
        self.menubar.add_command(label="Quit!", command=self.quit)

        self.filemenu = Menu(self.menubar)
        self.menubar.add_cascade(label="Des", menu=self.filemenu)
        self.filemenu.add_command(label="Nouveau", command=self.addDe)
        self.filemenu.add_separator()
        self.filemenu.add_command(label=u"Dé 4", command=self.addDen(4))
        self.filemenu.add_command(label=u"Dé 6", command=self.addDen(6))
        self.filemenu.add_command(label=u"Dé 8", command=self.addDen(8))
        self.filemenu.add_command(label=u"Dé 10", command=self.addDen(10))
        self.filemenu.add_command(label=u"Dé 12", command=self.addDen(12))
        self.filemenu.add_command(label=u"Dé 20", command=self.addDen(20))
        self.filemenu.add_command(label=u"Dé 24", command=self.addDen(24))
        self.filemenu.add_command(label=u"Dé 30", command=self.addDen(30))
        self.filemenu.add_command(label=u"Dé 100", command=self.addDen(100))
    
        #filemenu.add_command(label="Exit", command=root.quit)

        self.tiremenu = Menu(self.menubar)
        self.menubar.add_cascade(label="Lance", menu=self.tiremenu)
        self.tiremenu.add_command(label="Lance tous...", command=self.lancetous)
        #self.tiremenu.add_separator()
        
        #helpmenu = Menu(menu)
        #menu.add_cascade(label="Help", menu=helpmenu)
        #helpmenu.add_command(label="About...", command=About)



        # display the menu
        self.config(menu=self.menubar)

        
        if run:
            self.mainloop()

    def addDe(self,*e,**args):
        try:
            n=self.LD[-1].max
            #n=int(self.LD[-1].vm.get())
            print("n="+str(n))
        except:
            n=FenDes.currentnumber
        newD=De(n,root=self,pack=False)
        self.LD+=[newD]
        self.placegrille()

    def addDen(self,n="default"):
        #print(self.LD[-1].max)
        if n=="default":
            try:
                #print(self.LD[-1])
                n=self.LD[-1].max
                #print("n="+str(n))
                
                #n=int(self.LD[-1].vm.get())
            except:
                n=FenDes.currentnumber
        def f(*e):
            newD=De(n,root=self,pack=False)
            self.LD+=[newD]
            self.placegrille()
        return f


    def placegrille(self):
        for k in range(len(self.LD)):
            column=k%self.nbdesparligne
            row=1+int((k-column)/self.nbdesparligne)
            self.LD[k].grid(row=row,column=column)
            
            
            
    def closeDe(self,D):
        self.LD.remove(D)
        D.destroy()
        self.placegrille()
        
    def lancetous(self):
        for d in self.LD:
            d.lance()


# In[58]:


FenDes(run=True)


# In[34]:


#if __name__=="__main__":
#    De1=De(6)
#    De2=De(10,root=De1.root,run=True)


# In[ ]:




