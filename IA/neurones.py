
#%%

import numpy as np
# %%



def sigmoid(x):
    return 1/(1+np.exp(-x))

def dereivesigmoid(x):
    return np.exp(-x)/np.power(1+np.exp(-x),2)



class Neurone:

    def __init__(self,n,biais=0):
        self.n=n
        self.poids= (np.random.rand(n))*2-1
        self.biais=biais


    def __repr__(self):
        s=f"poids = {self.poids}\nbiais = {self.biais}"
        return s
    
    def predit(self,x):
        return sigmoid(self.poids.dot(x)-self.biais)
    
    def __call__(self,x):
        return self.predit(x)

    def backPropag(self,x,erreur,e=0.01):
        signe=1
        h=(self.poids.dot(x)-self.biais)
        g=dereivesigmoid(h)*2*erreur
        for i in range(len(self.poids)):
            self.poids[i]+=signe*g*x[i]*e
        self.biais+=-signe*g*e
        return g*self.poids
 
    def backPropagListe(self,Lx,Lerreur,e=0.01):
        signe=1
        L=[]
        for j in range(len(Lx)):
            h=(self.poids.dot(Lx[j])-self.biais)
            g=dereivesigmoid(h)*2*Lerreur[j]
            for i in range(len(self.poids)):
                self.poids[i]+=signe*g*Lx[j][i]*e
            self.biais+=-signe*g*e
            L+=[g*self.poids]
        return np.array(L)

#%%


class Couche:

    def __init__(self,entrees,sorties):
        self.neurones=[]
        for i in range(sorties):
            self.neurones+=[Neurone(entrees)]
        self.sorties=sorties
        self.entrees=entrees
    
    def __repr__(self):
        s=f"Nb entrees : {self.entrees}\nNb de sorties : {self.sorties}"
        return s
    
    def predit(self,x):
        return np.array([n(x) for n in self.neurones])

    def __call__(self,x):
        return self.predit(x)

    def backPropag(self,x,erreur,e=0.01):
        Ne=[0]*len(self.neurones)
        for i,n in enumerate(self.neurones):
            Ne[i]=n.backPropag(x,erreur[i],e)
        return np.array([sum([Ne[j][i] for j in range(len(erreur))]) for i in range(len(x))])
    

        


#%%


class Reseau:

    def __init__(self,tailles):
        self.tailles=tailles
        self.couches=[]
        for i in range(len(self.tailles)-1):
            self.couches+=[Couche(self.tailles[i],self.tailles[i+1])]
        
    def __repr__(self):
        s=f"Tailles des couches : {self.tailles}"
        return s

    def predit(self,x):
        y=x
        for i in range(len(self.tailles)-1):
            y=self.couches[i](y)
        return y

    def __call__(self,x):
        return self.predit(x)
          
    
    def backPropag(self,x,y,e=0.01):
        x=np.array(x)
        y=np.array(y)
        Lx=[x]
        Le=[x*0.]
        for c in self.couches:
            Lx+=[np.zeros(len(c.neurones))]
            Le+=[np.zeros(len(c.neurones))]
        for i,c in enumerate(self.couches):
            Lx[i+1]=c.predit(Lx[i])
        Le[len(self.couches)]=y-Lx[-1]
        for i in range(len(self.couches)-1,0,-1):
            c=self.couches[i]
            Le[i]=c.backPropag(Lx[i],Le[i+1],e)
        
    @property
    def Poids(self):
        for c in self.couches:
            print("---------")
            for n in c.neurones:
                print(n.poids,"\n")





#%%













# %%

#---------------------------------------------------------------------

N=Neurone(3)
print(N)
# %%

N.predit([4,-8,2])
# %%


C=Couche(3,9)
C.predit([2,-7,4.2])



# %%


R=Reseau([3,4,3,2])
print(R)
R([8,-5.72,2])


# %%

x=np.array([3.4,-6.23,2.1])
y=np.array([0.234,0.8832])



# %%

R(x)
# %%
erreur=np.dot(y-R(x),y-R(x))
print(erreur)
# %%

R.backPropag(x,y,8)
# %%
R.Poids

# %%
n=5
for i in range(n):
    R.backPropag(x,y,8)
erreur=np.dot(y-R(x),y-R(x))
print(erreur)

# %%
R.Poids
# %%










# %%

#Essais Entrainement
N=100
Lx=[]
Ly=[]
for i in range(N):
    x=np.random.rand(2)
    if x[0]**2+x[1]**2<=0.25:
        y=1.
    else:
        y=0.
    Lx+=[x]
    Ly+=[[y]]


# %%


R2=Reseau([2,5,2,1])

# %%
n=5
for i in range(n):
    erreur=0
    for j in range(N):
        R2.backPropag(Lx[j],Ly[j],0.5)
        erreur+=np.dot(Ly[j]-R2(Lx[j]),Ly[j]-R2(Lx[j]))
print(erreur)
# %%

# %%
